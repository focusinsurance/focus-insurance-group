Over the past twenty-five years Focus Insurance Group has evolved into what the modern insurance consumer requires. We serve as a link into the diverse world of insurance and financial products serving you, the customer. We are seasoned, licensed and experienced professionals.

Address: 2373 Lawrenceville Hwy, Decatur, GA 30033, USA

Phone: 404-633-3333